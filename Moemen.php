<?php

namespace Savvy\User\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// models
use Savvy\Branch\Branch;
use Savvy\Role\Role;
use Savvy\User\User;
use Sentinel;
use Activation;
use Carbon\Carbon;


class UserController extends Controller
{
  /**asdasdasd
    * 
    * dispaly a list of db records
    *
    * @return view 
    */
  public function index()
  {


   /* dd("kdsdfasdaslkdnjcbvnxcbvnbxnbvnxbcnvbxbcvnbxcbvxcbvbxvbxnbcvnxbvcbxcmvbnxbcvxbhhfhhhdhhdhncncjcncdncmmmnnnnnnnnnnnvn,g,,llvl,f"); my conflict line*/
    dd("kdjfbljsdf");
    echo "Please make a conflict ya Moemen";

    echo "hello";


    if (Sentinel::inRole('developer')) {
      dd("Asmaa");
        $users = User::active()
            ->get();
            echo "hello";
    }


"falkdhfjakhsdkjfakjhdfkjahkdjhfkajshdfkjahkjdfhkja"
    else {

      }

      echo "hello";
    dd("kdjfbljsdf");
        if (\PermissionHelper::hasSuperView('users')) {
            $users = User::where('id','!=',Sentinel::getUser()->id)
                ->active()
                ->get();

        } else {
//"khfdgjhsdljfgjsdhlfkghjklfhgkjldhfhgjkfhdgjhadfjgalkjhdljfahskljdfhalkjshdfljahsdfjahdfjhalkdhfkaljhdlkfjhadlkjfha"  my conflict line 
        dd("Asmaa");
        } else {
            $users = User::where('id','!=',Sentinel::getUser()->id)
                ->where('created_by', '=', Sentinel::check()->id)
                ->active()
                ->inSystem()
                ->get();
        }
"sdfhgfkjfgkhsfkjghkjfhgkhsfkjgskjhkgfksjhdfkjghksdfghskdjf"
    }

    dd("Asmaa");
    return view('users::list', compact('users'));
  }

  /**
    * create
    * 
    * dispaly view to insert new record
    *
    * @return view 
    */
  public function create()
  {
    $branches = Branch::all();
    $roles = Role::where(function ($query) {
        if (! Sentinel::inRole('developer')) {
            $query->where('slug', '!=', 'developer');
        }
    })->get();
    return view('users::create', compact('branches', 'roles'));
  }

  /**
    * store
    * 
    * save new inserted record
    *
    * @return redirect 
    */
  public function store(Request $request)
  {
      try {
          $rules = [
             'first_name' => 'required|min:3',
             'last_name' => 'required|min:3',
             'email' => 'required|email|unique:users',
             'password' => 'required|min:6',
             'branch_id' => 'required',
             'role_id' => 'required',
             'join_date' => 'date'
          ];


          $this->validate($request, $rules);

          $data = [
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'join_date' => ($request->input('join_date')) ? Carbon::parse($request->input('join_date'))->format('Y/m/d') : null,
            'created_by' => Sentinel::check()->id
          ];

          $user = Sentinel::register($data);

          $role = Sentinel::findRoleById($request->input('role_id'));
          $role->users()->attach($user);

          $user->branches()->sync($request->input('branch_id'));

          $activation = Activation::create($user);

          Activation::complete($user, $activation->code);
           
          return redirect()->route('user.list')->with('success', 'User has been added successfully!');
      } catch (\Illuminate\Database\QueryException $e) {
          return redirect()->back()->withInput()->with('message', 'User has not been added, Try again!');
      }
  }

  /**
    * edit
    * 
    * dispaly view to edit record
    *
    * @return view 
    */
  public function edit($id)
  {
    $branches = Branch::all();
    $roles = Role::where(function ($query) {
        if (! Sentinel::inRole('developer')) {
            $query->where('slug', '!=', 'developer');
        }
    })->get();
    $user = User::find($id);

    // has no permission to edit this record
    if(! \PermissionHelper::canEditRecord('users', $user->created_by)) {
        return redirect()->route('permissionDenied');
    }

    if($user->inRole('developer') && ! Sentinel::inRole('developer')) {
        return redirect()->route('permissionDenied');
    }

    return view('users::edit', compact('branches', 'roles', 'user'));
  }

  /**
    * update
    * 
    * update record
    *
    * @return redirect 
    */
  public function update(Request $request, $id)
  {
    try {
         $rules = [
          'first_name' => 'required|min:3',
          'last_name' => 'required|min:3',
          'email' => 'required|email|unique:users,email,' . $id,
          'branch_id' => 'required',
          'role_id' => 'required',
          'join_date' => 'date'
          ];

          $this->validate($request, $rules);

          $user = User::find($id);

          
          $data = [
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'join_date' => ($request->input('join_date')) ? Carbon::parse($request->input('join_date'))->format('Y/m/d') : null
          ];

          if( $request->input('password', '') != '' ) 
          {
            $data['password'] = Sentinel::getHasher()->hash( $request->input('password'));
          }
         
          $user->update($data);

          $role = Sentinel::findRoleById($request->input('role_id'));

          $user->roles()->sync([ 'role_id' => $role->id ]);

          $user->branches()->sync($request->input('branch_id'));


          return redirect()->route('user.list')->with('success', 'User has been updated successfully!');
      } catch (\Illuminate\Database\QueryException $e) {
          return redirect()->back()->withInput()->with('message', 'User not updated!');
      }
  }

  /**
    * deactivate
    * 
    * deactivate user
    *
    * @return redirect 
    */
  public function deactivate($id)
  {
    try {
        $user = User::find($id);

        if($user->inRole('developer') && ! Sentinel::inRole('developer')) {
            return redirect()->route('permissionDenied');
        }

        // check if user can't delete this recordd
        if (! \PermissionHelper::canDeleteRecord('users', $user->created_by)) {
            return redirect()->route('permissionDenied');
        }

        Activation::remove($user);

        $user->email = str_random(20) . '@almaqarr.com'; // change email
        $user->save();

        return redirect()->route('user.list')->with('success', 'User has been deleted successfully!');

       
    } catch (\Illuminate\Database\QueryException $e) {
        return redirect()->back()->withInput()->with('message', 'User has not been deleted, Try again!');
    }
  }


  /**
    * getActiveUserResponse
    * 
    * get the response of active users
    *
    * $request
    *
    * @return response 
    */
  public function getActiveUserResponse(Request $request)
  {
    $filter = "%" . $request->input('q', '') . '%';
    $module = $request->input('module', '');

    $users = User::where( function ($query) use ($module) {
      $query->active()
        ->inSystem();
    })->where( function ($query) use ($filter) {
      $query->where('first_name', 'like', $filter)
        ->orWhere('last_name','like', $filter)
        ->orWhere('email','like', $filter);
    })->get();
    
    $formatted_users = [];

    foreach ($users as $user) {
      if (! $module || $user->hasAccess('global.superadmin') || $user->hasAnyAccess($module . '.*')) {
        $formatted_users[] = ['id' => $user->id, 'text' => $user->fullNameEmailed];
      }
    }

    if ($request->input('select_all')) {
      array_unshift($formatted_users, ['id' => -1, 'text' => 'All']);
    }
    
    return Response()->json($formatted_users);
  }

}
