<?php

namespace Savvy\User\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// models
use Savvy\Branch\Branch;
use Savvy\Role\Role;
use Savvy\User\User;
use Sentinel;
use Activation;
use Carbon\Carbon;


class UserController extends Controller
{
  /**
    * index
    * 
    * dispaly a list of db records
    *
    * @return view 
    */
  public function index()
  {
    return "Does";
  }

  /**
    * create
    * 
    * dispaly view to insert new record
    *
    * @return view 
    */
  public function create()
  {
    $branches = Branch::all();
    $roles = Role::where(function ($query) {
        if (! Sentinel::inRole('developer')) {
            $query->where('slug', '!=', 'developer');
        }
    })->get();
    return view('users::create', compact('branches', 'roles'));
  }

}
